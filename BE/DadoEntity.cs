﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class DadoEntity
    {
        

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private bool seTira = false;

        public bool SeTira
        {
            get { return seTira; }
            set { seTira = value; }
        }


        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

    }
}
