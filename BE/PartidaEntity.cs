﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PartidaEntity
    {
        private JugadorEntity jugador;

        public JugadorEntity Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private List<MarcaEntity> marcasCompletadas = new List<MarcaEntity>();

        public List<MarcaEntity> MarcasCompletadas
        {
            get { return marcasCompletadas; }
            set { marcasCompletadas = value; }
        }

        private List<TurnoEntity> turnos = new List<TurnoEntity>();

        public List<TurnoEntity> Turnos
        {
            get { return turnos; }
            set { turnos = value; }
        }


        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private bool estaJugando;

        public bool EstaJugando
        {
            get { return estaJugando; }
            set { estaJugando = value; }
        }



    }
}
