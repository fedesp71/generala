﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class TurnoEntity
    {

        private List<DadoEntity> dados = new List<DadoEntity>();

        public List<DadoEntity> Dados
        {
            get { return dados; }
            set { dados = value; }
        }

        private int puntaje = 0;

        public int Puntaje
        {
            get { return puntaje; }
            set { puntaje = value; }
        }

        private int intento = 0;

        public int Intento
        {
            get { return intento; }
            set { intento = value; }
        }


    }
}
