﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class MarcaEntity
    {

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        private bool estaMarcada;

        public bool EstaMarcada
        {
            get { return estaMarcada; }
            set { estaMarcada = value; }
        }


    }
}
