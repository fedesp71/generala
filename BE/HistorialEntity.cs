﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class HistorialEntity
    {
        private string nombreJugador;

        public string NombreJugador
        {
            get { return nombreJugador; }
            set { nombreJugador = value; }
        }

        private int ganadas;

        public int Ganadas
        {
            get { return ganadas; }
            set { ganadas = value; }
        }

        private int empatadas;

        public int Empatadas
        {
            get { return empatadas; }
            set { empatadas = value; }
        }


        private int perdidas;

        public int Perdidas
        {
            get { return perdidas; }
            set { perdidas = value; }
        }

        private int jugadas;

        public int Jugadas
        {
            get { return jugadas; }
            set { jugadas = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }


    }
}
