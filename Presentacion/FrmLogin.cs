﻿using BE;
using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Presentacion
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        JugadorBusiness gestorJugador = new JugadorBusiness();
        internal List<JugadorEntity> Jugadores = new List<JugadorEntity>();

        private void BtnElegir_Click(object sender, EventArgs e)
        {

        }


        private void FrmLogin_Load(object sender, EventArgs e)
        {
            
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            
            if (ValidarCampos())
            {
                string claveEncriptada = gestorJugador.Encriptar(TxtClave.Text);
                if (gestorJugador.Validar(TxtClave.Text, claveEncriptada))
                {
                    CargarJugador(claveEncriptada);
                }
                else
                {
                    MessageBox.Show("Usuario y/o clave inválidos");
                }
            }

            if(Jugadores.Count == 2)
            {
                FrmPrincipal frmPrincipal = new FrmPrincipal();
                FrmTablero frmTablero = new FrmTablero();
                frmTablero.Jugadores = this.Jugadores;
                //frmTablero.MdiParent = this;
                frmTablero.Show();
                this.Close();
            }
        }

        public void CargarJugador(string claveEncriptada)
        {
            JugadorEntity jugador = new JugadorEntity();
            jugador.Usuario = TxtNombre.Text;
            jugador.Clave = claveEncriptada;
            Jugadores.Add(jugador);
            
            if(Jugadores.Count == 1)
            {
                MessageBox.Show("Usuario cargado, es necesario que cargue otro más");
                TxtNombre.Text = "";
                TxtClave.Text = "";
            }
        }

        public bool ValidarCampos()
        {
            if(TxtClave.Text == ""  || TxtClave.Text == "")
            {
                MessageBox.Show("Tiene que escribir sus credenciales");
                TxtNombre.Text = "";
                TxtClave.Text = "";
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
