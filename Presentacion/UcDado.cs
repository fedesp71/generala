﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class UcDado : UserControl
    {
        public UcDado()
        {
            InitializeComponent();
        }

        private bool marcado;

        public bool Marcado
        {
            get { return marcado; }
            set { marcado = value; }
        }



        public void EstablecerImagen(int valor)
        {
            switch(valor)
            {
                case 1:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado01.png");
                        break;
                    }
                case 2:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado02.png");
                        break;
                    }
                case 3:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado03.png");
                        break;
                    }
                case 4:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado04.png");
                        break;
                    }
                case 5:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado05.png");
                        break;
                    }
                case 6:
                    {
                        PbDado.Image = Image.FromFile("Imagenes\\Dado06.png");
                        break;
                    }
                default:
                    {
                        PbDado.Image = null;
                        break;
                    }
            }
        }

        public void Pintar(Color color)
        {
            pColor.BackColor = color;
        }

        private void PbDado_Click(object sender, EventArgs e)
        {
            Marcado = !Marcado;
            if(Marcado)
            {
                Pintar(Color.Red);
            }
            else
            {
                Pintar(Color.Black);
            }
            

        }

        private void UcDado_Load(object sender, EventArgs e)
        {
            Marcado = false;
            pColor.BackColor = Color.Black;
        }
    }
}
