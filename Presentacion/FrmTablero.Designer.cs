﻿namespace Presentacion
{
    partial class FrmTablero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnTirar = new System.Windows.Forms.Button();
            this.BtnTurno = new System.Windows.Forms.Button();
            this.dtMarcas = new System.Windows.Forms.DataGridView();
            this.Marcas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JugadorUno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JugadorDos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.LblJugadorTurno = new System.Windows.Forms.Label();
            this.LblJugador1 = new System.Windows.Forms.Label();
            this.LblJugador2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LblJugador1Turnos = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LblJugador2Turnos = new System.Windows.Forms.Label();
            this.ucDado5 = new Presentacion.UcDado();
            this.ucDado4 = new Presentacion.UcDado();
            this.ucDado3 = new Presentacion.UcDado();
            this.ucDado2 = new Presentacion.UcDado();
            this.ucDado1 = new Presentacion.UcDado();
            this.label2 = new System.Windows.Forms.Label();
            this.LblJugador1Puntos = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LblJugador2Puntos = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtMarcas)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnTirar
            // 
            this.BtnTirar.Location = new System.Drawing.Point(32, 514);
            this.BtnTirar.Name = "BtnTirar";
            this.BtnTirar.Size = new System.Drawing.Size(125, 59);
            this.BtnTirar.TabIndex = 5;
            this.BtnTirar.Text = "Tirar";
            this.BtnTirar.UseVisualStyleBackColor = true;
            this.BtnTirar.Click += new System.EventHandler(this.BtnTirar_Click);
            // 
            // BtnTurno
            // 
            this.BtnTurno.Location = new System.Drawing.Point(255, 514);
            this.BtnTurno.Name = "BtnTurno";
            this.BtnTurno.Size = new System.Drawing.Size(125, 59);
            this.BtnTurno.TabIndex = 6;
            this.BtnTurno.Text = "FinalizarTurno";
            this.BtnTurno.UseVisualStyleBackColor = true;
            this.BtnTurno.Click += new System.EventHandler(this.BtnTurno_Click);
            // 
            // dtMarcas
            // 
            this.dtMarcas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtMarcas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Marcas,
            this.JugadorUno,
            this.JugadorDos});
            this.dtMarcas.Location = new System.Drawing.Point(430, 181);
            this.dtMarcas.Name = "dtMarcas";
            this.dtMarcas.RowTemplate.Height = 24;
            this.dtMarcas.Size = new System.Drawing.Size(488, 392);
            this.dtMarcas.TabIndex = 7;
            this.dtMarcas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtMarcas_CellClick);
            // 
            // Marcas
            // 
            this.Marcas.HeaderText = "Column1";
            this.Marcas.Name = "Marcas";
            this.Marcas.ReadOnly = true;
            // 
            // JugadorUno
            // 
            this.JugadorUno.HeaderText = "Jug1";
            this.JugadorUno.Name = "JugadorUno";
            // 
            // JugadorDos
            // 
            this.JugadorDos.HeaderText = "Jug2";
            this.JugadorDos.Name = "JugadorDos";
            this.JugadorDos.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Turno de:";
            // 
            // LblJugadorTurno
            // 
            this.LblJugadorTurno.AutoSize = true;
            this.LblJugadorTurno.Location = new System.Drawing.Point(176, 29);
            this.LblJugadorTurno.Name = "LblJugadorTurno";
            this.LblJugadorTurno.Size = new System.Drawing.Size(0, 17);
            this.LblJugadorTurno.TabIndex = 9;
            // 
            // LblJugador1
            // 
            this.LblJugador1.AutoSize = true;
            this.LblJugador1.Location = new System.Drawing.Point(619, 29);
            this.LblJugador1.Name = "LblJugador1";
            this.LblJugador1.Size = new System.Drawing.Size(0, 17);
            this.LblJugador1.TabIndex = 10;
            // 
            // LblJugador2
            // 
            this.LblJugador2.AutoSize = true;
            this.LblJugador2.Location = new System.Drawing.Point(750, 29);
            this.LblJugador2.Name = "LblJugador2";
            this.LblJugador2.Size = new System.Drawing.Size(0, 17);
            this.LblJugador2.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(588, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Turno:";
            // 
            // LblJugador1Turnos
            // 
            this.LblJugador1Turnos.AutoSize = true;
            this.LblJugador1Turnos.Location = new System.Drawing.Point(640, 90);
            this.LblJugador1Turnos.Name = "LblJugador1Turnos";
            this.LblJugador1Turnos.Size = new System.Drawing.Size(0, 17);
            this.LblJugador1Turnos.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(728, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Turno:";
            // 
            // LblJugador2Turnos
            // 
            this.LblJugador2Turnos.AutoSize = true;
            this.LblJugador2Turnos.Location = new System.Drawing.Point(780, 90);
            this.LblJugador2Turnos.Name = "LblJugador2Turnos";
            this.LblJugador2Turnos.Size = new System.Drawing.Size(0, 17);
            this.LblJugador2Turnos.TabIndex = 15;
            // 
            // ucDado5
            // 
            this.ucDado5.Location = new System.Drawing.Point(217, 302);
            this.ucDado5.Marcado = false;
            this.ucDado5.Name = "ucDado5";
            this.ucDado5.Size = new System.Drawing.Size(79, 79);
            this.ucDado5.TabIndex = 4;
            // 
            // ucDado4
            // 
            this.ucDado4.Location = new System.Drawing.Point(104, 302);
            this.ucDado4.Marcado = false;
            this.ucDado4.Name = "ucDado4";
            this.ucDado4.Size = new System.Drawing.Size(79, 79);
            this.ucDado4.TabIndex = 3;
            // 
            // ucDado3
            // 
            this.ucDado3.Location = new System.Drawing.Point(301, 181);
            this.ucDado3.Marcado = false;
            this.ucDado3.Name = "ucDado3";
            this.ucDado3.Size = new System.Drawing.Size(79, 79);
            this.ucDado3.TabIndex = 2;
            // 
            // ucDado2
            // 
            this.ucDado2.Location = new System.Drawing.Point(163, 181);
            this.ucDado2.Marcado = false;
            this.ucDado2.Name = "ucDado2";
            this.ucDado2.Size = new System.Drawing.Size(79, 79);
            this.ucDado2.TabIndex = 1;
            // 
            // ucDado1
            // 
            this.ucDado1.Location = new System.Drawing.Point(32, 181);
            this.ucDado1.Marcado = false;
            this.ucDado1.Name = "ucDado1";
            this.ucDado1.Size = new System.Drawing.Size(79, 79);
            this.ucDado1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(590, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Puntos:";
            // 
            // LblJugador1Puntos
            // 
            this.LblJugador1Puntos.AutoSize = true;
            this.LblJugador1Puntos.Location = new System.Drawing.Point(646, 127);
            this.LblJugador1Puntos.Name = "LblJugador1Puntos";
            this.LblJugador1Puntos.Size = new System.Drawing.Size(0, 17);
            this.LblJugador1Puntos.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(728, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Puntos:";
            // 
            // LblJugador2Puntos
            // 
            this.LblJugador2Puntos.AutoSize = true;
            this.LblJugador2Puntos.Location = new System.Drawing.Point(790, 127);
            this.LblJugador2Puntos.Name = "LblJugador2Puntos";
            this.LblJugador2Puntos.Size = new System.Drawing.Size(0, 17);
            this.LblJugador2Puntos.TabIndex = 19;
            // 
            // FrmTablero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 599);
            this.Controls.Add(this.LblJugador2Puntos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LblJugador1Puntos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LblJugador2Turnos);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LblJugador1Turnos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LblJugador2);
            this.Controls.Add(this.LblJugador1);
            this.Controls.Add(this.LblJugadorTurno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtMarcas);
            this.Controls.Add(this.BtnTurno);
            this.Controls.Add(this.BtnTirar);
            this.Controls.Add(this.ucDado5);
            this.Controls.Add(this.ucDado4);
            this.Controls.Add(this.ucDado3);
            this.Controls.Add(this.ucDado2);
            this.Controls.Add(this.ucDado1);
            this.Name = "FrmTablero";
            this.Text = "Generala";
            this.Load += new System.EventHandler(this.FrmTablero_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtMarcas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UcDado ucDado1;
        private UcDado ucDado2;
        private UcDado ucDado3;
        private UcDado ucDado4;
        private UcDado ucDado5;
        private System.Windows.Forms.Button BtnTirar;
        private System.Windows.Forms.Button BtnTurno;
        private System.Windows.Forms.DataGridView dtMarcas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marcas;
        private System.Windows.Forms.DataGridViewTextBoxColumn JugadorUno;
        private System.Windows.Forms.DataGridViewTextBoxColumn JugadorDos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblJugadorTurno;
        private System.Windows.Forms.Label LblJugador1;
        private System.Windows.Forms.Label LblJugador2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblJugador1Turnos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LblJugador2Turnos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LblJugador1Puntos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblJugador2Puntos;
    }
}

