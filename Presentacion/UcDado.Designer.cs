﻿namespace Presentacion
{
    partial class UcDado
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PbDado = new System.Windows.Forms.PictureBox();
            this.pColor = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PbDado)).BeginInit();
            this.pColor.SuspendLayout();
            this.SuspendLayout();
            // 
            // PbDado
            // 
            this.PbDado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbDado.Location = new System.Drawing.Point(3, 3);
            this.PbDado.Name = "PbDado";
            this.PbDado.Size = new System.Drawing.Size(185, 133);
            this.PbDado.TabIndex = 0;
            this.PbDado.TabStop = false;
            this.PbDado.Click += new System.EventHandler(this.PbDado_Click);
            // 
            // pColor
            // 
            this.pColor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pColor.Controls.Add(this.PbDado);
            this.pColor.Location = new System.Drawing.Point(0, 0);
            this.pColor.Name = "pColor";
            this.pColor.Size = new System.Drawing.Size(217, 169);
            this.pColor.TabIndex = 1;
            // 
            // UcDado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pColor);
            this.Name = "UcDado";
            this.Size = new System.Drawing.Size(217, 169);
            this.Load += new System.EventHandler(this.UcDado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PbDado)).EndInit();
            this.pColor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PbDado;
        private System.Windows.Forms.Panel pColor;
    }
}
