﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmHistorial : Form
    {
        public FrmHistorial()
        {
            InitializeComponent();
        }
        HistorialBusiness gestorHistorial = new HistorialBusiness();

        private void FrmHistorial_Load(object sender, EventArgs e)
        {
            LlenarGrillaHistorial();
        }

        public void LlenarGrillaHistorial()
        {
            DtHistorial.DataSource = null;
            DtHistorial.DataSource = gestorHistorial.Listar();
        }
    }
}
