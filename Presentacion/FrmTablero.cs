﻿using BE;
using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmTablero : Form
    {
        public FrmTablero()
        {
            InitializeComponent();
        }
        internal List<JugadorEntity> Jugadores = new List<JugadorEntity>();


        PartidaEntity partida1 = new PartidaEntity();
        PartidaEntity partida2 = new PartidaEntity();
        TurnoBusiness gestorTurno = new TurnoBusiness();
        MarcaBusiness gestorMarcas = new MarcaBusiness();
        PartidaBusiness gestorPartida = new PartidaBusiness();
        private void FrmTablero_Load(object sender, EventArgs e)
        {
            partida1.Jugador = Jugadores[0];
            partida1.Puntos = 0;
            partida1.EstaJugando = true;
            partida2.Jugador = Jugadores[1];
            partida2.Puntos = 0;
            partida2.EstaJugando = false;
            TurnoEntity turno = new TurnoEntity();
            partida1.Turnos.Add(turno);

            CargarTablaMarca();

            LblJugadorTurno.Text = Jugadores[0].Usuario;
            LblJugador1.Text = Jugadores[0].Usuario;
            LblJugador2.Text = Jugadores[1].Usuario;
            LblJugador1Puntos.Text = "0";
            LblJugador2Puntos.Text = "0";
            LblJugador1Turnos.Text = partida1.Turnos.Count.ToString();
            LblJugador2Turnos.Text = partida2.Turnos.Count.ToString();
        }

        public void CargarTablaMarca()
        {
            dtMarcas.Rows.Add("1", "", "");
            dtMarcas.Rows.Add("2", "", "");
            dtMarcas.Rows.Add("3", "", "");
            dtMarcas.Rows.Add("4", "", "");
            dtMarcas.Rows.Add("5", "", "");
            dtMarcas.Rows.Add("6", "", "");
            dtMarcas.Rows.Add("Doble", "", "");
            dtMarcas.Rows.Add("Escalera", "", "");
            dtMarcas.Rows.Add("Full", "", "");
            dtMarcas.Rows.Add("Poker", "", "");
            dtMarcas.Rows.Add("Generala", "", "");
            dtMarcas.Rows.Add("Generala Doble", "", "");

            dtMarcas.Columns[1].HeaderText = Jugadores[0].Usuario;
            dtMarcas.Columns[2].HeaderText = Jugadores[1].Usuario;
        }

        private void BtnTirar_Click(object sender, EventArgs e)
        {
            BorrarMarcasNoSeleccionadas();
            if (partida1.EstaJugando)
            {
                partida1 = Tirar(partida1);
            }
            if (partida2.EstaJugando)
            {
                partida2 = Tirar(partida2);
            }
            ucDado1.Marcado = false;
            ucDado2.Marcado = false;
            ucDado3.Marcado = false;
            ucDado4.Marcado = false;
            ucDado5.Marcado = false;

        }

        public PartidaEntity Tirar(PartidaEntity partida)
        {
            if (partida.Turnos[partida.Turnos.Count - 1].Intento == 0)
            {
                //Primera vez que se tira en el turno
                partida.Turnos[partida.Turnos.Count - 1] = PrimeraTirada(partida.Turnos[partida.Turnos.Count - 1]);

                List<MarcaEntity> marcasDisponibles = new List<MarcaEntity>();

                marcasDisponibles = gestorMarcas.CalculaTipoTirada(partida.Turnos[partida.Turnos.Count - 1], partida.Jugador, partida.MarcasCompletadas);
                PonerMarcasDisponibles(marcasDisponibles, partida.Jugador);
            }
            else //Segundo o tercer intento
            {
                //cambio los dados marcados
                partida.Turnos[partida.Turnos.Count - 1].Dados[0] = CambiarDadoMarcado(ucDado1, partida.Turnos[partida.Turnos.Count - 1], 0);
                partida.Turnos[partida.Turnos.Count - 1].Dados[1] = CambiarDadoMarcado(ucDado2, partida.Turnos[partida.Turnos.Count - 1], 1);
                partida.Turnos[partida.Turnos.Count - 1].Dados[2] = CambiarDadoMarcado(ucDado3, partida.Turnos[partida.Turnos.Count - 1], 2);
                partida.Turnos[partida.Turnos.Count - 1].Dados[3] = CambiarDadoMarcado(ucDado4, partida.Turnos[partida.Turnos.Count - 1], 3);
                partida.Turnos[partida.Turnos.Count - 1].Dados[4] = CambiarDadoMarcado(ucDado5, partida.Turnos[partida.Turnos.Count - 1], 4);

                //tiro los dados
                partida.Turnos[partida.Turnos.Count - 1].Dados = gestorTurno.TirarDados(partida.Turnos[partida.Turnos.Count - 1].Dados);

                //cambio la imagen con los nuevos valores
                CambiarImagenDados(partida.Turnos[partida.Turnos.Count - 1].Dados);

                List<MarcaEntity> marcasDisponibles = new List<MarcaEntity>();
                marcasDisponibles = gestorMarcas.CalculaTipoTirada(partida.Turnos[partida.Turnos.Count - 1], partida.Jugador, partida.MarcasCompletadas);
                PonerMarcasDisponibles(marcasDisponibles, partida.Jugador);
                partida.Turnos[partida.Turnos.Count - 1].Intento++;
                DesmarcarDados();
            }
            if (partida.Turnos[partida.Turnos.Count - 1].Intento >= 3)
            {
                BtnTirar.Enabled = false;
            }
            return partida;
        }

        private DadoEntity CambiarDadoMarcado(UcDado dado,TurnoEntity turno, int nroDado)
        {
            if (dado.Marcado)
            {
                turno.Dados[nroDado].SeTira = true;
            }
            return turno.Dados[nroDado];
        }

        public void LimpiarDados()
        {
            ucDado1.EstablecerImagen(0);
            ucDado2.EstablecerImagen(0);
            ucDado3.EstablecerImagen(0);
            ucDado4.EstablecerImagen(0);
            ucDado5.EstablecerImagen(0);
        }
        private void CambiarImagenDados(List<DadoEntity> dados)
        {
            foreach (DadoEntity dadoAux in dados)
            {
                switch (dadoAux.Id)
                {
                    case 1:
                        ucDado1.EstablecerImagen(dadoAux.Valor);
                        break;
                    case 2:
                        ucDado2.EstablecerImagen(dadoAux.Valor);
                        break;
                    case 3:
                        ucDado3.EstablecerImagen(dadoAux.Valor);
                        break;
                    case 4:
                        ucDado4.EstablecerImagen(dadoAux.Valor);
                        break;
                    case 5:
                        ucDado5.EstablecerImagen(dadoAux.Valor);
                        break;
                }
            }
        }

        private void BtnTurno_Click(object sender, EventArgs e)
        {
            CambioTurno();
        }

        public void CambioTurno()
        {
            TurnoEntity turno = new TurnoEntity();
            if (partida1.EstaJugando)
            {
                partida2.Turnos.Add(turno);
                partida1.EstaJugando = false;
                partida2.EstaJugando = true;
                LblJugadorTurno.Text = partida2.Jugador.Usuario;
                LblJugador2Turnos.Text = partida2.Turnos.Count.ToString();
            }
            else
            {
                partida1.Turnos.Add(turno);
                partida2.EstaJugando = false;
                partida1.EstaJugando = true;
                LblJugadorTurno.Text = partida1.Jugador.Usuario;
                LblJugador1Turnos.Text = partida1.Turnos.Count.ToString();
            }
            BtnTirar.Enabled = true;


            if (partida1.Turnos.Count == 11)
            {
                if (partida1.Puntos > partida2.Puntos)
                {
                    gestorPartida.RegistrarGanador(partida1);
                    gestorPartida.RegistrarPerdedor(partida2);
                    MessageBox.Show($"Ganó {partida1.Jugador.Usuario} con {partida1.Puntos.ToString()} puntos");
                }
                if (partida1.Puntos < partida2.Puntos)
                {
                    gestorPartida.RegistrarGanador(partida2);
                    gestorPartida.RegistrarPerdedor(partida1);
                    MessageBox.Show($"Ganó {partida2.Jugador.Usuario} con {partida2.Puntos.ToString()} puntos");
                }
                if (partida1.Puntos == partida2.Puntos)
                {
                    gestorPartida.RegistrarEmpate(partida1);
                    gestorPartida.RegistrarEmpate(partida2);
                    MessageBox.Show($"{partida1.Jugador.Usuario} y {partida2.Jugador.Usuario} empataron con {partida2.Puntos.ToString()} puntos ");
                }

                BtnTirar.Enabled = false;
            }
        }

        public void DesmarcarDados()
        {
            ucDado1.Pintar(Color.Black);
            ucDado2.Pintar(Color.Black);
            ucDado3.Pintar(Color.Black);
            ucDado4.Pintar(Color.Black);
            ucDado5.Pintar(Color.Black);
        }

        public TurnoEntity PrimeraTirada(TurnoEntity turno)
        {
            turno.Dados = gestorTurno.TirarDadosPrimeraVez();
            CambiarImagenDados(turno.Dados);
            turno.Intento++;
            return turno;
        }


        public void PonerMarcasDisponibles(List<MarcaEntity> marcas, JugadorEntity jugador)
        {
            int x;
            if(jugador.Usuario == dtMarcas.Columns[1].HeaderText)
            {
                x = 1;
            }
            else
            {
                x = 2;
            }
            foreach(MarcaEntity marcaAux in marcas)
            {
                int index = (from r in dtMarcas.Rows.Cast<DataGridViewRow>()
                             where r.Cells[0].Value.ToString() == marcaAux.Nombre
                             select r.Index).First();
                dtMarcas.Rows[index].Cells[x].Style.BackColor = Color.Yellow;
                dtMarcas.Rows[index].Cells[x].Value = marcaAux.Valor;
            }
        }

        public void BorrarMarcasNoSeleccionadas()
        {
            foreach (DataGridViewRow row in dtMarcas.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Style.BackColor == Color.Yellow)
                    {
                        cell.Style.BackColor = Color.White;
                        cell.Value = "";
                    }
                }
            }
        }

        private void dtMarcas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if(dtMarcas.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.Yellow)
            {
                dtMarcas.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.GreenYellow;
                MarcaEntity marca = new MarcaEntity();
                marca.Valor = int.Parse(dtMarcas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                marca.Nombre = dtMarcas.Rows[e.RowIndex].Cells[0].Value.ToString();
                marca.EstaMarcada = true;
                if(e.ColumnIndex == 1)
                {
                    partida1.MarcasCompletadas.Add(marca);
                    partida1.Puntos += marca.Valor;
                    LblJugador1Puntos.Text = partida1.Puntos.ToString();
                }
                else
                {
                    partida2.MarcasCompletadas.Add(marca);
                    partida2.Puntos += marca.Valor;
                    LblJugador2Puntos.Text = partida2.Puntos.ToString();
                }
                BorrarMarcasNoSeleccionadas();
                CambioTurno();
                LimpiarDados();
            }
            
        }
    }
}
