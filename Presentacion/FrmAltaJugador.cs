﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmAltaJugador : Form
    {
        JugadorBusiness gestorJugador = new JugadorBusiness();
        public FrmAltaJugador()
        {
            InitializeComponent();
        }

        private void BtnAgregarUsuario_Click(object sender, EventArgs e)
        {
            if(TxtClave2.Text != "")
            {
                gestorJugador.Agregar(TxtNombre.Text, gestorJugador.Encriptar(TxtClave2.Text));

                LblUsuarioAgregado.Text = "Usuario Agregado";
                LblUsuarioAgregado.BackColor = Color.GreenYellow;
                TxtNombre.Enabled = false;
                TxtClave1.Enabled = false;
                TxtClave2.Enabled = false;
                LblUsuarioAgregado.Enabled = false;

                System.Threading.Thread.Sleep(4000);

                TxtNombre.Text = "";
                TxtClave1.Text = "";
                TxtClave2.Text = "";
                TxtNombre.Enabled = true;
                TxtClave1.Enabled = true;
                TxtClave2.Enabled = true;
                LblUsuarioAgregado.Text = "";
                LblUsuarioDisponible.Text = "";
            }
            
        }

        private void TxtNombre_Leave(object sender, EventArgs e)
        {
            //bool usuarioDisponible = gestorJugador.NombreUsuarioDisponible(TxtNombre.Text)
            if(gestorJugador.NombreUsuarioDisponible(TxtNombre.Text))
            {
                LblUsuarioDisponible.Text = "Nombre de usuario disponible";
                LblUsuarioDisponible.BackColor = Color.GreenYellow;
                BtnAgregarUsuario.Enabled = true;
            }
            else
            {
                LblUsuarioDisponible.Text = "Nombre no disponible";
                LblUsuarioDisponible.BackColor = Color.Red;
                BtnAgregarUsuario.Enabled = false;
            }
        }

        private void TxtClave2_Leave(object sender, EventArgs e)
        {
            if(TxtClave1.Text != TxtClave2.Text)
            {
                LblClave.Text = "las claves no coinciden";
                LblClave.BackColor = Color.Red;
                BtnAgregarUsuario.Enabled = false;
            }
            else
            {
                LblClave.Text = "";
                LblClave.BackColor = Color.Red;
                BtnAgregarUsuario.Enabled = true;
            }
        }
    }
}
