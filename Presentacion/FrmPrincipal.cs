﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistorial frmHistorial = new FrmHistorial();
            frmHistorial.MdiParent = this;
            frmHistorial.Show();
        }

        private void jugarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.MdiParent = this;
            frmLogin.Show();
        }

        private void crearJugadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAltaJugador frmAltaJugador = new FrmAltaJugador();
            frmAltaJugador.MdiParent = this;
            frmAltaJugador.Show();
        }
    }
}
