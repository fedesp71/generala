﻿namespace Presentacion
{
    partial class FrmAltaJugador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtClave2 = new System.Windows.Forms.TextBox();
            this.TxtClave1 = new System.Windows.Forms.TextBox();
            this.BtnAgregarUsuario = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LblUsuarioDisponible = new System.Windows.Forms.Label();
            this.LblClave = new System.Windows.Forms.Label();
            this.LblUsuarioAgregado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(121, 21);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(125, 22);
            this.TxtNombre.TabIndex = 0;
            this.TxtNombre.Leave += new System.EventHandler(this.TxtNombre_Leave);
            // 
            // TxtClave2
            // 
            this.TxtClave2.Location = new System.Drawing.Point(121, 121);
            this.TxtClave2.Name = "TxtClave2";
            this.TxtClave2.PasswordChar = '*';
            this.TxtClave2.Size = new System.Drawing.Size(125, 22);
            this.TxtClave2.TabIndex = 2;
            this.TxtClave2.Leave += new System.EventHandler(this.TxtClave2_Leave);
            // 
            // TxtClave1
            // 
            this.TxtClave1.Location = new System.Drawing.Point(121, 74);
            this.TxtClave1.Name = "TxtClave1";
            this.TxtClave1.PasswordChar = '*';
            this.TxtClave1.Size = new System.Drawing.Size(125, 22);
            this.TxtClave1.TabIndex = 1;
            // 
            // BtnAgregarUsuario
            // 
            this.BtnAgregarUsuario.Location = new System.Drawing.Point(12, 176);
            this.BtnAgregarUsuario.Name = "BtnAgregarUsuario";
            this.BtnAgregarUsuario.Size = new System.Drawing.Size(234, 54);
            this.BtnAgregarUsuario.TabIndex = 3;
            this.BtnAgregarUsuario.Text = "Agregar";
            this.BtnAgregarUsuario.UseVisualStyleBackColor = true;
            this.BtnAgregarUsuario.Click += new System.EventHandler(this.BtnAgregarUsuario_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Clave";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Repetir Clave";
            // 
            // LblUsuarioDisponible
            // 
            this.LblUsuarioDisponible.AutoSize = true;
            this.LblUsuarioDisponible.Location = new System.Drawing.Point(276, 21);
            this.LblUsuarioDisponible.Name = "LblUsuarioDisponible";
            this.LblUsuarioDisponible.Size = new System.Drawing.Size(0, 17);
            this.LblUsuarioDisponible.TabIndex = 7;
            // 
            // LblClave
            // 
            this.LblClave.AutoSize = true;
            this.LblClave.Location = new System.Drawing.Point(276, 79);
            this.LblClave.Name = "LblClave";
            this.LblClave.Size = new System.Drawing.Size(0, 17);
            this.LblClave.TabIndex = 8;
            // 
            // LblUsuarioAgregado
            // 
            this.LblUsuarioAgregado.AutoSize = true;
            this.LblUsuarioAgregado.Location = new System.Drawing.Point(276, 176);
            this.LblUsuarioAgregado.Name = "LblUsuarioAgregado";
            this.LblUsuarioAgregado.Size = new System.Drawing.Size(0, 17);
            this.LblUsuarioAgregado.TabIndex = 9;
            // 
            // FrmAltaJugador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 252);
            this.Controls.Add(this.LblUsuarioAgregado);
            this.Controls.Add(this.LblClave);
            this.Controls.Add(this.LblUsuarioDisponible);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnAgregarUsuario);
            this.Controls.Add(this.TxtClave1);
            this.Controls.Add(this.TxtClave2);
            this.Controls.Add(this.TxtNombre);
            this.Name = "FrmAltaJugador";
            this.Text = "FrmAltaJugador";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtClave2;
        private System.Windows.Forms.TextBox TxtClave1;
        private System.Windows.Forms.Button BtnAgregarUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblUsuarioDisponible;
        private System.Windows.Forms.Label LblClave;
        private System.Windows.Forms.Label LblUsuarioAgregado;
    }
}