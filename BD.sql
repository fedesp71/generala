USE [master]
GO
/****** Object:  Database [Generala]    Script Date: 22/11/2020 11:06:49 ******/
CREATE DATABASE [Generala]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Generala', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Generala.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Generala_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Generala_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Generala] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Generala].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Generala] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Generala] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Generala] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Generala] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Generala] SET ARITHABORT OFF 
GO
ALTER DATABASE [Generala] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Generala] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Generala] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Generala] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Generala] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Generala] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Generala] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Generala] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Generala] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Generala] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Generala] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Generala] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Generala] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Generala] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Generala] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Generala] SET RECOVERY FULL 
GO
ALTER DATABASE [Generala] SET  MULTI_USER 
GO
ALTER DATABASE [Generala] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Generala] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Generala] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Generala] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Generala] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Generala', N'ON'
GO
ALTER DATABASE [Generala] SET QUERY_STORE = OFF
GO
USE [Generala]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Generala]
GO
/****** Object:  Table [dbo].[tHistorial]    Script Date: 22/11/2020 11:06:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tHistorial](
	[NombreJugador] [varchar](50) NOT NULL,
	[PartidasGanadas] [int] NULL,
	[PartidasEmpatadas] [int] NULL,
	[PartidasPerdidas] [int] NULL,
	[PartidasJugadas] [int] NULL,
	[TotalPuntos] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NombreJugador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tJugador]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tJugador](
	[Nombre] [varchar](50) NOT NULL,
	[Clave] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tHistorial]  WITH CHECK ADD FOREIGN KEY([NombreJugador])
REFERENCES [dbo].[tJugador] ([Nombre])
GO
/****** Object:  StoredProcedure [dbo].[spCrearJugador]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCrearJugador] 
@Nombre varchar(50),
@Clave varchar(50)
AS

	BEGIN TRANSACTION;

	BEGIN TRY
		INSERT INTO tJugador values (@Nombre, @Clave)
		INSERT INTO tHistorial values(@Nombre, 0, 0, 0,0, 0)
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION;
	END CATCH;
	
GO
/****** Object:  StoredProcedure [dbo].[spGuardarHistorial]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGuardarHistorial] 
@Nombre varchar(50),
@Ganada int,
@Empatada int,
@Derrota int,
@Puntos int
AS

	BEGIN TRANSACTION;

	BEGIN TRY
		update tHistorial set 
		PartidasGanadas  += @Ganada,
		PartidasEmpatadas  += @Empatada, 
		PartidasPerdidas += @Derrota,
		PartidasJugadas  += 1,
		TotalPuntos      += @Puntos
		where NombreJugador = @Nombre
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION;
	END CATCH;
	
GO
/****** Object:  StoredProcedure [dbo].[spListarJugadoresLogin]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spListarJugadoresLogin]
AS
	select Nombre, Clave from tJugador
GO
/****** Object:  StoredProcedure [dbo].[spNombreUsuarioDisponible]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spNombreUsuarioDisponible]
@Nombre varchar(50)

AS
	SELECT Nombre  FROM tJugador 
	WHERE Nombre = @Nombre
GO
/****** Object:  StoredProcedure [dbo].[spObtenerHistorial]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spObtenerHistorial] 

AS
	select NombreJugador, PartidasGanadas, PartidasEmpatadas,PartidasPerdidas, PartidasJugadas, TotalPuntos from tHistorial
	
GO
/****** Object:  StoredProcedure [dbo].[spValidarJugador]    Script Date: 22/11/2020 11:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidarJugador] 
@Nombre varchar(50),
@Clave varchar(50)
AS
	SELECT Nombre, Clave  FROM tJugador 
	WHERE Nombre = @Nombre and Clave = @Clave
GO
USE [master]
GO
ALTER DATABASE [Generala] SET  READ_WRITE 
GO
