﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class McPartida
    {
        private Acceso acceso = new Acceso();

        public void RegistrarGanador(PartidaEntity ganador)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", ganador.Jugador.Usuario));
            parameters.Add(acceso.CrearParametro("@Ganada", 1));
            parameters.Add(acceso.CrearParametro("@Empatada", 0));
            parameters.Add(acceso.CrearParametro("@Derrota", 0));
            parameters.Add(acceso.CrearParametro("@Puntos", ganador.Puntos));
            acceso.Escribir("spGuardarHistorial", parameters);

            acceso.Cerrar();
        }

        public void RegistrarPerdedor(PartidaEntity perdedor)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", perdedor.Jugador.Usuario));
            parameters.Add(acceso.CrearParametro("@Ganada", 0));
            parameters.Add(acceso.CrearParametro("@Empatada", 0));
            parameters.Add(acceso.CrearParametro("@Derrota", 1));
            parameters.Add(acceso.CrearParametro("@Puntos", perdedor.Puntos));
            acceso.Escribir("spGuardarHistorial", parameters);

            acceso.Cerrar();
        }

        public void RegistrarEmpate(PartidaEntity empate)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", empate.Jugador.Usuario));
            parameters.Add(acceso.CrearParametro("@Ganada", 0));
            parameters.Add(acceso.CrearParametro("@Empatada", 1));
            parameters.Add(acceso.CrearParametro("@Derrota", 0));
            parameters.Add(acceso.CrearParametro("@Puntos", empate.Puntos));
            acceso.Escribir("spGuardarHistorial", parameters);

            acceso.Cerrar();
        }
    }
}
