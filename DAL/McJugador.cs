﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class McJugador
    {
        Acceso acceso = new Acceso();

        public List<JugadorEntity> ListarLogin()
        {

            List<JugadorEntity> jugadores = new List<JugadorEntity>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarJugadoresLogin");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                JugadorEntity jugador = new JugadorEntity();
                jugador.Usuario = registro["Nombre"].ToString();
                jugador.Clave = registro["Clave"].ToString();

                jugadores.Add(jugador);
            }
            return jugadores;
        }   
        
        public bool ValidarJugador(string nombre, string clave)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", nombre));
            parameters.Add(acceso.CrearParametro("@Clave", clave));

            DataTable tabla = acceso.Leer("spValidarJugador", parameters);
            acceso.Cerrar();

            if(tabla.Rows.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            } 
        }

        public bool NombreUsuarioDisponible(string nombre)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", nombre));

            DataTable tabla = acceso.Leer("spNombreUsuarioDisponible", parameters);
            acceso.Cerrar();

            if (tabla.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Agregar(string nombre, string clave)
        {
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", nombre));
            parameters.Add(acceso.CrearParametro("@Clave", clave));

            return  acceso.Escribir("spCrearJugador", parameters);



        }
    }
}
