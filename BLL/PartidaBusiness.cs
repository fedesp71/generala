﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PartidaBusiness
    {
        McPartida mj = new McPartida();
        public void RegistrarGanador(PartidaEntity ganador)
        {
            mj.RegistrarGanador(ganador);
        }

        public void RegistrarPerdedor(PartidaEntity perdedor)
        {
            mj.RegistrarPerdedor(perdedor);
        }

        public void RegistrarEmpate(PartidaEntity empate)
        {
            mj.RegistrarEmpate(empate);
        }

    }
}
