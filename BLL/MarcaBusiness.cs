﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class MarcaBusiness
    {
        public List<MarcaEntity> CalculaTipoTirada(TurnoEntity tirada, JugadorEntity jugador, List<MarcaEntity> marcasCompletadas)
        {
            List<MarcaEntity> marcasPuntos = new List<MarcaEntity>();

            int cantDado1 = 0;
            int cantDado2 = 0;
            int cantDado3 = 0;
            int cantDado4 = 0;
            int cantDado5 = 0;
            int cantDado6 = 0;

            foreach (DadoEntity dado in tirada.Dados)
            {
                switch (dado.Valor)
                {
                    case 1:
                        cantDado1++;
                        break;
                    case 2:
                        cantDado2++;
                        break;
                    case 3:
                        cantDado3++;
                        break;
                    case 4:
                        cantDado4++;
                        break;
                    case 5:
                        cantDado5++;
                        break;
                    case 6:
                        cantDado6++;
                        break;

                }
            }
            List<int> nroDados = new List<int>();
            nroDados.Add(cantDado1);
            nroDados.Add(cantDado2);
            nroDados.Add(cantDado3);
            nroDados.Add(cantDado4);
            nroDados.Add(cantDado5);
            nroDados.Add(cantDado6);

            int doble = 0;
            int escalera = 0;
            int full = 0;
            int poker = 0;
            int generala = 0;

            foreach (int nro in nroDados)
            {
                if (nro == 1)
                {
                    escalera++;
                }
                if (nro == 2)
                {
                    doble++;
                    full++;
                }
                if (nro == 3)
                {
                    full++;
                }
                if (nro == 4)
                {
                    poker++;
                }
                if (nro == 5)
                {
                    generala++;
                }
            }
            if (MarcaDisponible(marcasCompletadas, "1"))
            {
                marcasPuntos.Add(MarcaNro(cantDado1, 1));
            }

            if (MarcaDisponible(marcasCompletadas, "2"))
            {
                marcasPuntos.Add(MarcaNro(cantDado2, 2));
            }

            if (MarcaDisponible(marcasCompletadas, "3"))
            {
                marcasPuntos.Add(MarcaNro(cantDado3, 3));
            }

            if (MarcaDisponible(marcasCompletadas, "4"))
            {
                marcasPuntos.Add(MarcaNro(cantDado4, 4));
            }

            if (MarcaDisponible(marcasCompletadas, "5"))
            {
                marcasPuntos.Add(MarcaNro(cantDado5, 5));
            }

            if (MarcaDisponible(marcasCompletadas, "6"))
            {
                marcasPuntos.Add(MarcaNro(cantDado6, 6));
            }

            if (doble == 2 && MarcaDisponible(marcasCompletadas, "Doble"))
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Doble", 15));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Doble", 10));
                }
            }
            if (full == 2 && MarcaDisponible(marcasCompletadas, "Full"))
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Full", 35));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Full", 30));
                }
            }
            if (poker == 1 && MarcaDisponible(marcasCompletadas, "Poker"))
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Poker", 45));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Poker", 40));
                }
            }
            if (generala == 1)
            {
                if(marcasCompletadas.Count > 0)
                {
                    if(marcasCompletadas[marcasCompletadas.Count -1].Nombre == "Generala")
                    {
                        marcasPuntos.Add(MarcaTipo("Generala Doble", 100));
                    }
                    else
                    {
                        marcasPuntos.Add(MarcaTipo("Generala", 50));
                    }
                }
               

            }
            
            if (cantDado1 == 1 && cantDado2 == 1 && cantDado3 == 1 && cantDado4 == 1 && cantDado5 == 1)
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 25));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 20));
                }
            }

            if (cantDado2 == 1 && cantDado3 == 1 && cantDado4 == 1 && cantDado5 == 1 && cantDado6 == 1)
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 25));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 20));
                }
            }

            if (cantDado3 == 1 && cantDado4 == 1 && cantDado5 == 1 && cantDado6 == 1 && cantDado1 == 1)
            {
                if (tirada.Intento == 1)
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 25));
                }
                else
                {
                    marcasPuntos.Add(MarcaTipo("Escalera", 20));
                }
            }

            return marcasPuntos;

        }

        public MarcaEntity MarcaNro(int cantDado, int num)
        {
            MarcaEntity marcaNro = new MarcaEntity();
            marcaNro.Nombre = num.ToString();
            marcaNro.Valor = cantDado * num;
            return marcaNro;
        }

        public MarcaEntity MarcaTipo(string nombre, int valor)
        {
            MarcaEntity marcaTipo = new MarcaEntity();
            marcaTipo.Nombre = nombre;
            marcaTipo.Valor = valor;
            return marcaTipo;
        }


        public bool MarcaDisponible(List<MarcaEntity> marcas, string marca)
        {
            foreach(MarcaEntity marcaAux in marcas)
            {
                if(marcaAux.Nombre == marca)
                {
                    return false;
                }
            }

            return true;
            
        }
    }


}
