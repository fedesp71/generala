﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TurnoBusiness
    {
        public List<DadoEntity> TirarDados(List<DadoEntity> dados)
        {
            Random rnd = new Random();
            foreach(DadoEntity dado in dados)
            {
                if(dado.SeTira)
                {
                    dado.Valor = rnd.Next(1, 7);
                    dado.SeTira = false;
                }  
            }
            return dados;
        }

        public List<DadoEntity> TirarDadosPrimeraVez()
        {
            List<DadoEntity> dados = new List<DadoEntity>();
            Random rnd = new Random();
            DadoEntity dado1 = new DadoEntity();
            DadoEntity dado2 = new DadoEntity();
            DadoEntity dado3 = new DadoEntity();
            DadoEntity dado4 = new DadoEntity();
            DadoEntity dado5 = new DadoEntity();

            dado1.Id = 1;
            dado1.Valor = rnd.Next(1, 7);

            dado2.Id = 2;
            dado2.Valor = rnd.Next(1, 7);

            dado3.Id = 3;
            dado3.Valor = rnd.Next(1, 7);

            dado4.Id = 4;
            dado4.Valor = rnd.Next(1, 7);

            dado5.Id = 5;
            dado5.Valor = rnd.Next(1, 7);
            
            dados.Add(dado1);
            dados.Add(dado2);
            dados.Add(dado3);
            dados.Add(dado4);
            dados.Add(dado5);

            return dados;
        }
    }
}
